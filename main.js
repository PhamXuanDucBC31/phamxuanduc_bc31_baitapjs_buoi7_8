// input
var num = [];
document.getElementById("add_data").addEventListener("click", function () {
  let data = document.getElementById("data_input").value * 1;
  num.push(data);
  console.log(num);
  document.getElementById("data_show").innerHTML = "";
  for (var i = 0; i < num.length; i++) {
    var div = document.createElement("div");
    div.style.width = "40px";
    div.style.height = "40px";
    div.style.color = "#black";
    div.style.marginRight = "15px";
    div.style.marginLeft = "20px";
    div.innerHTML = num[i];
    div.style.fontSize = "30px";
    document.getElementById("data_show").appendChild(div);
  }
});

//global funtion
function prime_test(x) {
  var S = 0;
  for (var i = 2; i < x; i++) {
    if (parseInt(x % i) == 0) {
      S = S + 1;
    }
  }
  return S;
}

// bài 1
document.getElementById("ex_out_1").style.display = "none";
document.getElementById("ex_1").addEventListener("click", function () {
  const targetDiv = document.getElementById("ex_out_1");
  if (targetDiv.style.display === "none") {
    targetDiv.style.display = "block";
  } else {
    targetDiv.style.display = "none";
  }
});
document.getElementById("sum").addEventListener("click", function () {
  var S = 0;
  for (var i = 0; i <= num.length - 1; i++) {
    if (num[i] > 0) {
      S = S + num[i];
      min_ex3 = num[i];
    }
  }
  document.getElementById(
    "result_sum"
  ).innerHTML = `<div> Tổng số dương là : ${S} </div>`;
});

// bài 2
document.getElementById("ex_out_2").style.display = "none";
document.getElementById("ex_2").addEventListener("click", function () {
  const targetDiv = document.getElementById("ex_out_2");
  console.log(targetDiv.style.display);
  if (targetDiv.style.display === "none") {
    targetDiv.style.display = "block";
  } else {
    targetDiv.style.display = "none";
  }
});
document.getElementById("count").addEventListener("click", function () {
  console.log(num.length);
  var Count = 0;
  for (var i = 0; i <= num.length - 1; i++) {
    if (num[i] > 0) {
      Count++;
    }
  }
  document.getElementById(
    "result_count"
  ).innerHTML = `<div> Có ${Count} số dương </div>`;
});

// bài 3
document.getElementById("ex_out_3").style.display = "none";
document.getElementById("ex_3").addEventListener("click", function () {
  const targetDiv = document.getElementById("ex_out_3");
  console.log(targetDiv.style.display);
  if (targetDiv.style.display === "none") {
    targetDiv.style.display = "block";
  } else {
    targetDiv.style.display = "none";
  }
});
document.getElementById("min").addEventListener("click", function () {
  console.log(num.length);
  var Min = num[0];
  for (var i = 1; i <= num.length - 1; i++) {
    if (num[i] < Min) {
      Min = num[i];
    }
  }
  document.getElementById(
    "result_min"
  ).innerHTML = `<div> Số nhỏ nhất là: ${Min} </div>`;
});

// bài 4
document.getElementById("ex_out_4").style.display = "none";
document.getElementById("ex_4").addEventListener("click", function () {
  const targetDiv = document.getElementById("ex_out_4");
  console.log(targetDiv.style.display);
  if (targetDiv.style.display === "none") {
    targetDiv.style.display = "block";
  } else {
    targetDiv.style.display = "none";
  }
});
document.getElementById("positive_min").addEventListener("click", function () {
  for (var i = 0; i < num.length; i++) {
    if (num[i] > 0) {
      var P_Min = num[i];
    }
  }
  for (var i = 0; i <= num.length - 1; i++) {
    if (num[i] > 0) {
      if (P_Min > num[i]) {
        P_Min = num[i];
      }
    }
  }
  document.getElementById(
    "result_positive_min"
  ).innerHTML = `<div> Số dương nhỏ nhất là : ${P_Min} </div>`;
});

// bài 5
document.getElementById("ex_out_5").style.display = "none";
document.getElementById("ex_5").addEventListener("click", function () {
  const targetDiv = document.getElementById("ex_out_5");
  console.log(targetDiv.style.display);
  if (targetDiv.style.display === "none") {
    targetDiv.style.display = "block";
  } else {
    targetDiv.style.display = "none";
  }
});
document
  .getElementById("last_even_number")
  .addEventListener("click", function () {
    var last_enum = 0;
    for (var i = 1; i <= num.length - 1; i++) {
      if (parseInt(num[i]) % 2 == 0) {
        last_enum = num[i];
      }
    }
    document.getElementById(
      "result_last_even_number"
    ).innerHTML = `<div> Số chẵn cuối cùng là : ${last_enum} </div>`;
  });

// bài 6
document.getElementById("ex_out_6").style.display = "none";
document.getElementById("ex_6").addEventListener("click", function () {
  const targetDiv = document.getElementById("ex_out_6");
  console.log(targetDiv.style.display);
  if (targetDiv.style.display === "none") {
    targetDiv.style.display = "block";
  } else {
    targetDiv.style.display = "none";
  }
});
document
  .getElementById("change_position")
  .addEventListener("click", function () {
    let pos_1 = document.getElementById("position_1").value * 1;
    let pos_2 = document.getElementById("position_2").value * 1;
    var num_temp = num[pos_1];
    num[pos_1] = num[pos_2];
    num[pos_2] = num_temp;
    document.getElementById(
      "result_change_position"
    ).innerHTML = `<div>Mảng sau khi đổi chỗ là: ${num}</div>`;
  });

// bài 7
document.getElementById("ex_out_7").style.display = "none";
document.getElementById("ex_7").addEventListener("click", function () {
  const targetDiv = document.getElementById("ex_out_7");
  console.log(targetDiv.style.display);
  if (targetDiv.style.display === "none") {
    targetDiv.style.display = "block";
  } else {
    targetDiv.style.display = "none";
  }
});
document.getElementById("sortting").addEventListener("click", function () {
  for (var i = 0; i < num.length; i++) {
    for (j = i + 1; j < num.length; j++) {
      if (num[i] > num[j]) {
        var temp = num[i];
        num[i] = num[j];
        num[j] = temp;
      }
    }
  }
  document.getElementById("result_sortting").innerHTML = `<div>${num}</div>`;
});

//bài 8
document.getElementById("ex_out_8").style.display = "none";
document.getElementById("ex_8").addEventListener("click", function () {
  const targetDiv = document.getElementById("ex_out_8");
  console.log(targetDiv.style.display);
  if (targetDiv.style.display === "none") {
    targetDiv.style.display = "block";
  } else {
    targetDiv.style.display = "none";
  }
});
document.getElementById("prime_num").addEventListener("click", function () {
  for (var i = 0; i < num.length; i++) {
    if (Number.isInteger(num[i]) == true && num[i] > 1) {
      var p = prime_test(num[i]);
      if (p == 0) {
        document.getElementById(
          "result_prime_num"
        ).innerHTML = `<div>Số nguyên tố đầu tiên là: ${num[i]}</div>`;
        break;
      } else {
        document.getElementById("result_prime_num").innerHTML = `<div>-1</div>`;
      }
    } else {
      document.getElementById("result_prime_num").innerHTML = `<div>-1</div>`;
    }
  }
});

//bài 9
document.getElementById("ex_out_9").style.display = "none";
document.getElementById("ex_9").addEventListener("click", function () {
  const targetDiv = document.getElementById("ex_out_9");
  console.log(targetDiv.style.display);
  if (targetDiv.style.display === "none") {
    targetDiv.style.display = "block";
  } else {
    targetDiv.style.display = "none";
  }
});
//thêm số thực
document.getElementById("add_real_num").addEventListener("click", function () {
  let data = document.getElementById("real_data_input").value * 1;
  num.push(data);
  document.getElementById("data_show_ex9").innerHTML = "";
  for (var i = 0; i < num.length; i++) {
    var div = document.createElement("div");
    div.style.width = "40px";
    div.style.height = "40px";
    div.style.color = "#black";
    div.style.marginRight = "15px";
    div.style.marginLeft = "20px";
    div.innerHTML = num[i];
    div.style.fontSize = "30px";
    document.getElementById("data_show_ex9").appendChild(div);
  }
});
document.getElementById("integer_count").addEventListener("click", function () {
  var count = 0;
  for (var i = 0; i < num.length; i++) {
    if (Number.isInteger(num[i]) == true) {
      var count = count + 1;
    }
  }
  document.getElementById(
    "result_integer_count"
  ).innerHTML = `<div>Mảng mới thêm có: ${count} số nguyên<div>`;
});

//bài 10
document.getElementById("ex_out_10").style.display = "none";
document.getElementById("ex_10").addEventListener("click", function () {
  const targetDiv = document.getElementById("ex_out_10");
  console.log(targetDiv.style.display);
  if (targetDiv.style.display === "none") {
    targetDiv.style.display = "block";
  } else {
    targetDiv.style.display = "none";
  }
});
document.getElementById("compare").addEventListener("click", function () {
  var negative = 0;
  var positive = 0;
  for (var i = 0; i < num.length; i++) {
    if (num[i] > 0) {
      positive++;
    }
    if (num[i] < 0) {
      negative++;
    }
  }
  if (negative > positive) {
    document.getElementById(
      "result_compare"
    ).innerHTML = `<div>Số dương (${positive}) < Số âm (${negative})<div>`;
  }
  if (negative < positive) {
    document.getElementById(
      "result_compare"
    ).innerHTML = `<div>Số dương (${positive}) > Số âm (${negative})<div>`;
  }
  if (negative == positive) {
    document.getElementById(
      "result_compare"
    ).innerHTML = `<div>Số dương (${positive}) = Số âm (${negative})<div>`;
  }
});
